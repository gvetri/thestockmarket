package com.example.giuseppe.stockmarket.domain.model

//todo this class should be the mapped class from the service, this class should be used for Room for example
data class ResponseDataEntity (val id : Int, val name : String, val ric: String, val sharePrice : Float, val description : String = "", val country : String = "")