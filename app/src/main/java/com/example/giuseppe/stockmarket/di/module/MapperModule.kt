package com.example.giuseppe.stockmarket.di.module

import com.example.giuseppe.stockmarket.data.network.mappers.MainDataMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MapperModule {

    @Provides
    @Singleton
    fun provideMainMapper() : MainDataMapper{
        return MainDataMapper()
    }
}