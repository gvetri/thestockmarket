package com.example.giuseppe.stockmarket.di.module

import com.example.giuseppe.stockmarket.di.scopes.ForActivity
import com.example.giuseppe.stockmarket.ui.activities.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @ForActivity
    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun provideMain(): MainActivity

}
