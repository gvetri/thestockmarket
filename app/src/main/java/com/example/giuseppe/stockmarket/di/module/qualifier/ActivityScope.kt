package com.example.giuseppe.stockmarket.di.module.qualifier

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope
