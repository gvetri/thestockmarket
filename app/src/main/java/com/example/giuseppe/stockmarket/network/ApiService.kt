package com.example.giuseppe.stockmarket.network

import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import io.reactivex.Flowable
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @get:GET("companies")
    val getCompanies: Flowable<List<CompanyResponse>>

    @GET("companies/{companyId}")
    fun getCompaniesById(@Path("companyId") id: Int): Flowable<CompanyResponse>
}
