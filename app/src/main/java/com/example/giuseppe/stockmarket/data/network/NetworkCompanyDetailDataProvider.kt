package com.example.giuseppe.stockmarket.data.network

import com.example.giuseppe.stockmarket.data.datasources.CompanyDetailProvider
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import com.example.giuseppe.stockmarket.network.ApiService
import io.reactivex.Flowable
import javax.inject.Inject

class NetworkCompanyDetailDataProvider @Inject constructor(var apiService: ApiService) : CompanyDetailProvider {
    override fun getCompanyDetailData(id : Int): Flowable<CompanyResponse> {
        return apiService.getCompaniesById(id)
    }
}