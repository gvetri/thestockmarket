package com.example.giuseppe.stockmarket.usecases

import com.example.giuseppe.stockmarket.data.network.repository.MainRepository
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import io.reactivex.Flowable
import javax.inject.Inject

class MainUseCase @Inject constructor(val mainRepository: MainRepository) : BaseUseCase() {

    fun getData(): Flowable<List<CompanyResponse>> {
        return mainRepository.getData()
    }

}