package com.example.giuseppe.stockmarket.data.network.mappers

interface Mapper<M,T> {
    fun map(entry : M): T?
}