package com.example.giuseppe.stockmarket.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import com.example.giuseppe.stockmarket.usecases.DetailUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CompanyDetailViewModel @Inject constructor(var detailUseCase: DetailUseCase) : BaseViewModel() {

    private var privateDetailLiveData = MutableLiveData<CompanyResponse>()

    private val disposables = CompositeDisposable()

    fun getCompanyDetail(id: Int): MutableLiveData<CompanyResponse> {
        disposables.add(getData(id))
        return privateDetailLiveData
    }

    fun getData(id: Int): Disposable {
        return detailUseCase.getCompanyDetailData(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    privateDetailLiveData.value = it
                }
    }


}