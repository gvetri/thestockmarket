package com.example.giuseppe.stockmarket.data.network.repository

import com.example.giuseppe.stockmarket.data.datasources.CompanyDetailProvider
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import io.reactivex.Flowable
import javax.inject.Inject

class CompanyDetailRepository @Inject constructor(var companyDetailProvider: CompanyDetailProvider){

    fun getData(id : Int): Flowable<CompanyResponse> {
        return companyDetailProvider.getCompanyDetailData(id)
    }

}