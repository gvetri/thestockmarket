package com.example.giuseppe.stockmarket.data.network.repository

import com.example.giuseppe.stockmarket.data.datasources.MainProvider
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import io.reactivex.Flowable
import javax.inject.Inject

class MainRepository @Inject constructor(var mainProvider: MainProvider){

   fun getData(): Flowable<List<CompanyResponse>> {
        return mainProvider.getData()
    }

}

