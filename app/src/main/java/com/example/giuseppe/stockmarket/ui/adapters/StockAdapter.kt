package com.example.giuseppe.stockmarket.ui.adapters

import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.giuseppe.stockmarket.R
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse


class StockAdapter(private val stockList: List<CompanyResponse>, private val listener: StockAdapter.OnClickListener) : RecyclerView.Adapter<StockAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        var stockName: TextView = itemView.findViewById(R.id.tvName)
        var stockRic: TextView = itemView.findViewById(R.id.tvRic)
        var stockPrice: TextView = itemView.findViewById(R.id.tvPrice)
        var cardView: CardView = itemView.findViewById(R.id.cardview)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.stock_row, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.stockName.text = stockList[position].name
        holder.stockRic.text = stockList[position].ric
        holder.stockPrice.text = stockList[position].sharePrice.toString()
        holder.cardView.setOnClickListener { listener.onClickItemListener(stockList[position]) }
    }

    override fun getItemCount(): Int {
        return stockList.size
    }

    interface OnClickListener {
        fun onClickItemListener(company: CompanyResponse)
    }
}