package com.example.giuseppe.stockmarket.ui.base

import android.annotation.SuppressLint
import android.arch.lifecycle.LifecycleRegistry
import android.support.v7.app.AppCompatActivity

//todo we use base Classes in case we need to share some logic between all the children of this class
@SuppressLint("Registered")
class BaseActivity : AppCompatActivity() {
    val lifecycle = LifecycleRegistry(this)
}
