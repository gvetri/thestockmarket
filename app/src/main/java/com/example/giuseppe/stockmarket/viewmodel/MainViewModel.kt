package com.example.giuseppe.stockmarket.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import com.example.giuseppe.stockmarket.usecases.MainUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class MainViewModel @Inject constructor(var mainUseCase: MainUseCase) : BaseViewModel() {

    private var privateExampleMutableLiveData = MutableLiveData<List<CompanyResponse>>()

    private val disposables = CompositeDisposable()

    val exampleData: MutableLiveData<List<CompanyResponse>>
        get() {
            getStockList()
            return privateExampleMutableLiveData
        }


    fun getStockList() {
        disposables.add(getData())
    }

    //todo created a disposable which is going to repeat every 20 seconds and updating the liveData
    fun getData(): Disposable {
        return mainUseCase.getData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .repeatWhen { it.delay(20, TimeUnit.SECONDS) }
            .subscribe {
                privateExampleMutableLiveData.value = it
            }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }


}