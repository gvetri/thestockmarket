package com.example.giuseppe.stockmarket.ui.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.adapters.ViewBindingAdapter.setClickListener
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.giuseppe.stockmarket.R
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import com.example.giuseppe.stockmarket.databinding.FragmentMainBinding
import com.example.giuseppe.stockmarket.di.Injectable
import com.example.giuseppe.stockmarket.ui.adapters.StockAdapter
import com.example.giuseppe.stockmarket.ui.base.BaseFragment
import com.example.giuseppe.stockmarket.viewmodel.MainViewModel
import com.example.giuseppe.stockmarket.viewmodel.ViewModelFactory
import timber.log.Timber
import javax.inject.Inject


class MainFragment : BaseFragment(), Injectable {

    @set:Inject
    var viewModelFactory: ViewModelFactory? = null

    private lateinit var mainViewModel: MainViewModel
    private lateinit var binding: FragmentMainBinding
    lateinit var recyclerView: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        recyclerView = binding.recyclerview
        recyclerView.layoutManager = LinearLayoutManager(context)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainViewModel.exampleData.observe(this, Observer {
            if (it != null) {
                Toast.makeText(context, "Updating..", Toast.LENGTH_LONG).show()
                val adapter = StockAdapter(it, setAdapterClickListener())
                recyclerView.adapter = adapter
            } else {
                Timber.d("Ha ocurrido un error")
            }
        })
    }

    private fun setAdapterClickListener(): StockAdapter.OnClickListener {
        return object : StockAdapter.OnClickListener {
            override fun onClickItemListener(company: CompanyResponse) {
                Toast.makeText(context, "hello", Toast.LENGTH_SHORT).show()
                val bundle = Bundle()
                bundle.putParcelable("company",company)
                findNavController().navigate(R.id.secondFragment,bundle)
            }

        }
    }


}