package com.example.giuseppe.stockmarket.di.module

import com.example.giuseppe.stockmarket.data.datasources.CompanyDetailProvider
import com.example.giuseppe.stockmarket.data.network.NetworkCompanyDetailDataProvider
import com.example.giuseppe.stockmarket.data.network.NetworkMainDataProvider
import com.example.giuseppe.stockmarket.data.network.mappers.MainDataMapper
import com.example.giuseppe.stockmarket.network.ApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataProviderModule {
    @Provides
    @Singleton
    fun provideDataProvider(apiService: ApiService,mainDataMapper: MainDataMapper): NetworkMainDataProvider {
        return NetworkMainDataProvider(apiService,mainDataMapper)
    }

    @Provides
    @Singleton
    fun provideCompanyDetailProvider(apiService: ApiService): CompanyDetailProvider {
        return NetworkCompanyDetailDataProvider(apiService)
    }
}