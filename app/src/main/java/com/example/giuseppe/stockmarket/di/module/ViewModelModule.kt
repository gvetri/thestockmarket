package com.example.giuseppe.stockmarket.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.giuseppe.stockmarket.di.module.qualifier.ViewModelKey
import com.example.giuseppe.stockmarket.viewmodel.CompanyDetailViewModel
import com.example.giuseppe.stockmarket.viewmodel.MainViewModel
import com.example.giuseppe.stockmarket.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CompanyDetailViewModel::class)
    abstract fun bindCompanyDetailViewModel(viewModel: CompanyDetailViewModel): ViewModel

}
