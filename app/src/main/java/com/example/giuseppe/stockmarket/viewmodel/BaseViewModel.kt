package com.example.giuseppe.stockmarket.viewmodel

import android.arch.lifecycle.ViewModel

open class BaseViewModel : ViewModel()