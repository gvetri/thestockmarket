package com.example.giuseppe.stockmarket.usecases

import com.example.giuseppe.stockmarket.data.network.repository.CompanyDetailRepository
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import io.reactivex.Flowable
import javax.inject.Inject

class DetailUseCase @Inject constructor(val companyDetailRepository : CompanyDetailRepository) : BaseUseCase() {

    fun getCompanyDetailData(id : Int): Flowable<CompanyResponse> {
        return companyDetailRepository.getData(id)
    }
}