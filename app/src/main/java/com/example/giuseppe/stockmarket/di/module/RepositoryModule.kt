package com.example.giuseppe.stockmarket.di.module

import com.example.giuseppe.stockmarket.data.network.NetworkCompanyDetailDataProvider
import com.example.giuseppe.stockmarket.data.network.repository.MainRepository
import com.example.giuseppe.stockmarket.data.network.NetworkMainDataProvider
import com.example.giuseppe.stockmarket.data.network.repository.CompanyDetailRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideMainRepository(networkMainDataProvider: NetworkMainDataProvider) : MainRepository {
        return MainRepository(networkMainDataProvider)
    }

    @Provides
    @Singleton
    fun provideCompanyDetailRepository(companyDetailDataProvider: NetworkCompanyDetailDataProvider) : CompanyDetailRepository {
        return CompanyDetailRepository(companyDetailDataProvider)
    }

}
