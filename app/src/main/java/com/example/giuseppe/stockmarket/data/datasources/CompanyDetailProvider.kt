package com.example.giuseppe.stockmarket.data.datasources

import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import io.reactivex.Flowable

interface CompanyDetailProvider {
    fun getCompanyDetailData(id : Int): Flowable<CompanyResponse>
}