package com.example.giuseppe.stockmarket.di.module

import com.example.giuseppe.stockmarket.data.network.repository.CompanyDetailRepository
import com.example.giuseppe.stockmarket.data.network.repository.MainRepository
import com.example.giuseppe.stockmarket.usecases.DetailUseCase
import com.example.giuseppe.stockmarket.usecases.MainUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCasesModule {

    @Provides
    @Singleton
    fun provideMainUseCase(mainRepository: MainRepository) : MainUseCase{
        return MainUseCase(mainRepository)
    }

    @Provides
    @Singleton
    fun provideCompanyDetailUseCase(companyDetailRepository: CompanyDetailRepository) : DetailUseCase{
        return DetailUseCase(companyDetailRepository)
    }

}