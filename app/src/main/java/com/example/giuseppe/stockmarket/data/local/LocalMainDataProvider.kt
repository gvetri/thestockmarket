package com.example.giuseppe.stockmarket.data.local

import com.example.giuseppe.stockmarket.data.datasources.MainProvider
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import io.reactivex.Flowable

//todo retrieve the local data source from example: Room,Realm
//todo in a really pragmatic clean architecture it should be inside the framework layer because it calls framework classes
class LocalMainDataProvider : MainProvider {
    override fun getData(): Flowable<List<CompanyResponse>> {
        //todo this could be used to load data from .json mocking class or from database
        TODO("not implemented")
    }


}