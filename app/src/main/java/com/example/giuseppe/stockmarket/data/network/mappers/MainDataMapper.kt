package com.example.giuseppe.stockmarket.data.network.mappers

import android.arch.lifecycle.MutableLiveData
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import com.example.giuseppe.stockmarket.domain.model.ResponseDataEntity

//todo this is a custom mapper in case we need to parse data from the service to the UI
class MainDataMapper : Mapper<MutableLiveData<List<CompanyResponse>>, MutableLiveData<List<ResponseDataEntity>>> {

    override fun map(entry: MutableLiveData<List<CompanyResponse>>): MutableLiveData<List<ResponseDataEntity>> {
        val dataEntityLiveData = MutableLiveData<List<ResponseDataEntity>>()
        val dataEntityList = ArrayList<ResponseDataEntity>()
        entry.value?.let {
            it.forEach { response ->
                val dataEntity = ResponseDataEntity(response.id, response.name, response.ric, response.sharePrice)
                dataEntityList.add(dataEntity)
            }
        }
        dataEntityLiveData.value = dataEntityList
        return dataEntityLiveData
    }

}