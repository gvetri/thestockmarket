package com.example.giuseppe.stockmarket.ui.fragments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.giuseppe.stockmarket.R
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import com.example.giuseppe.stockmarket.databinding.FragmentCompanyDetailBinding
import com.example.giuseppe.stockmarket.di.Injectable
import com.example.giuseppe.stockmarket.ui.base.BaseFragment
import com.example.giuseppe.stockmarket.viewmodel.CompanyDetailViewModel
import com.example.giuseppe.stockmarket.viewmodel.ViewModelFactory
import javax.inject.Inject


class CompanyDetailFragment : BaseFragment(), Injectable {

    @set:Inject
    var viewModelFactory: ViewModelFactory? = null

    private lateinit var viewModel: CompanyDetailViewModel
    private lateinit var binding: FragmentCompanyDetailBinding

    private lateinit var selectedCompany: CompanyResponse

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_company_detail, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CompanyDetailViewModel::class.java)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectedCompany = arguments?.get("company") as CompanyResponse
        viewModel.getCompanyDetail(selectedCompany.id).observe(this, Observer {
            it?.let { company -> setCompanyDetailInfo(company) }
        })
    }

    private fun setCompanyDetailInfo(company: CompanyResponse) {
        binding.tvSharePrice.text = company.sharePrice.toString()
        binding.tvName.text = company.name
        binding.tvDescription.text = company.description
        binding.tvCountry.text = company.country
        binding.tvBic.text = company.ric
    }
}
