package com.example.giuseppe.stockmarket.di.component

import com.example.giuseppe.stockmarket.App
import com.example.giuseppe.stockmarket.di.module.*
import com.example.giuseppe.stockmarket.di.module.network.GsonModule
import com.example.giuseppe.stockmarket.di.module.network.OkHttpInterceptorsModule
import com.example.giuseppe.stockmarket.ui.activities.MainActivity
import com.example.giuseppe.stockmarket.ui.fragments.CompanyDetailFragment
import com.example.giuseppe.stockmarket.ui.fragments.CompanyDetailFragment_MembersInjector
import com.example.giuseppe.stockmarket.ui.fragments.MainFragment
import com.example.giuseppe.stockmarket.usecases.DetailUseCase
import com.example.giuseppe.stockmarket.usecases.MainUseCase
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    AndroidModule::class,
    GsonModule::class,
    OkHttpInterceptorsModule::class,
    UseCasesModule::class,
    DataProviderModule::class,
    RepositoryModule::class,
    MapperModule::class,
    ViewModelModule::class,
    MainActivityModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        fun appModule(appModule: AppModule): Builder

        fun okHttpInterceptorsModule(okHttpInterceptorsModule: OkHttpInterceptorsModule): Builder

        fun gsonModule(gsonModule: GsonModule): Builder

        fun androidModule(androidModule: AndroidModule): Builder
    }

    fun inject(application: App)

    fun inject(mainActivity: MainActivity)

    fun inject(mainFragment: MainFragment)

    fun inject(companyDetailFragment : CompanyDetailFragment)

    fun getMainUseCase() : MainUseCase

    fun getCompanyDetailUseCase(): DetailUseCase

}
