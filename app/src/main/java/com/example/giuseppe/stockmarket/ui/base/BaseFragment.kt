package com.example.giuseppe.stockmarket.ui.base

import android.arch.lifecycle.LifecycleRegistry
import android.support.v4.app.Fragment

open class BaseFragment : Fragment() {
    var lifecycleRegistry = LifecycleRegistry(this)

}