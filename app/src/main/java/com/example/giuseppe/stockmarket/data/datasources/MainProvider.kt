package com.example.giuseppe.stockmarket.data.datasources

import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import io.reactivex.Flowable

//todo this source is implemented in the repository which you're going to need the data
interface MainProvider {
    fun getData(): Flowable<List<CompanyResponse>>
}