package com.example.giuseppe.stockmarket.data.network.response

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CompanyResponse (@SerializedName("id") @Expose val id : Int,
                            @SerializedName("name") @Expose val name : String,
                            @SerializedName("ric") @Expose val ric: String,
                            @SerializedName("sharePrice") @Expose val sharePrice : Float,
                            @SerializedName("description") @Expose  val description : String,
                            @SerializedName("country") @Expose val country : String) : Parcelable