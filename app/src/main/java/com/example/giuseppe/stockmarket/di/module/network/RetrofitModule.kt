package com.example.giuseppe.stockmarket.di.module.network

import com.example.giuseppe.stockmarket.BuildConfig
import com.example.giuseppe.stockmarket.network.ApiService
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import android.support.test.espresso.IdlingResource
import com.jakewharton.espresso.OkHttp3IdlingResource


@Module
class RetrofitModule {

    @Provides
    @Singleton
    fun getApiInterface(retroFit: Retrofit): ApiService {
        return retroFit.create<ApiService>(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun getRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.ENDPOINT)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun getOkHttpCleint(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        val client = OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
        if (BuildConfig.DEBUG)
            IdlingResources.registerOkHttp(client)
        return client
    }

}