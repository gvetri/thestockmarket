package com.example.giuseppe.stockmarket.data.network

import com.example.giuseppe.stockmarket.data.datasources.MainProvider
import com.example.giuseppe.stockmarket.data.network.mappers.MainDataMapper
import com.example.giuseppe.stockmarket.data.network.response.CompanyResponse
import com.example.giuseppe.stockmarket.network.ApiService
import io.reactivex.Flowable
import javax.inject.Inject

//todo get the data from network using this provider
//todo inject the mapper in case we want to transform data from API to UI
class NetworkMainDataProvider @Inject constructor(var apiService: ApiService, var dataMapper: MainDataMapper) : MainProvider {

    override fun getData(): Flowable<List<CompanyResponse>> {
        return apiService.getCompanies
    }
}