package com.example.giuseppe.stockmarket.di.module

import com.example.giuseppe.stockmarket.ui.fragments.CompanyDetailFragment
import com.example.giuseppe.stockmarket.ui.fragments.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract fun contributeMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributeCompanyDetailFragment(): CompanyDetailFragment
}
